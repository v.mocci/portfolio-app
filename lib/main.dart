import 'BHBusRecharge/pages/bhbuscardlist.dart';
import 'LineSearch/pages/linesearch.dart';
import 'Profile/pages/showprofile.dart';
import 'RechargePlaces/pages/rechargeplacesmap.dart';
import 'RouteSearch/pages/routesearch.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  int _selectedPage = 0;
  final _pageOptions = [
    RoutePage(),
    LinesPage(),
    RechargePage(),
    RechargePlacesPage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BHBUS+',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        
        body: _pageOptions[_selectedPage],
        bottomNavigationBar: new Theme(
          data: Theme.of(context).copyWith(
            canvasColor: const Color(0xFF8E4A7E),
          ),
          child: new BottomNavigationBar(
            currentIndex: _selectedPage,
            onTap: (int index) {
              setState(() {
                _selectedPage = index;
              });
            },
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.directions_bus), 
                  title: Text('Trajetos')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.linear_scale), 
                  title: Text('Linhas')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.account_balance_wallet),
                  title: Text('Cartões')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.location_on), 
                  title: Text('Postos')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.account_circle), 
                  title: Text('Perfil')),
            ],
            selectedItemColor: Colors.white,
            unselectedItemColor: Colors.grey[300],
          ),
        ),
      ),
    );
  }
}


// Tutorials used for this project:

// Bottom NavBar: https://www.youtube.com/watch?v=n_FRmFm9Tyw
// Maps: 
