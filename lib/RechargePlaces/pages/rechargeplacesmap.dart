import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
//import '../../Resources/classes/main.class.dart';

class RechargePlacesPage extends StatelessWidget {
 GoogleMapController mapController;

  final LatLng _center = const LatLng(-19.912998, -43.940933);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
       appBar: AppBar(
             centerTitle: false,
             title: new Text("Postos de Recarga"),
            backgroundColor: Color.fromRGBO(142, 74, 126, 1.0),
            elevation: 50.0,
          ),
             body: GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 15.0,
              ),
          mapType: MapType.normal,
         ),
      ),
    );
  }
}
