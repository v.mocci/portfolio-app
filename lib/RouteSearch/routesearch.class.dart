import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'routesearch.variables.dart';

//ROUTES PAGE`S START//

AppBar routesAppBar() {
  return new AppBar(
            title:           routesAppBarTitle,
            backgroundColor: routesAppBarbkColor,
            elevation:       routesElevation,
            );
}

GoogleMap mainMap() {

  GoogleMapController mapController;
  final LatLng _center = routesMapsLatLng;
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    }

return new GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
          target: _center,
          zoom: routesMapZoom),
          mapType: routesMapType,
          );
        }

//ROUTES PAGE`S END//