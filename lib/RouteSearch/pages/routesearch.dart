import 'package:flutter/material.dart';
import '../routesearch.class.dart';

class RoutePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: routesAppBar(),
      body: mainMap(),
    );
  }
}

