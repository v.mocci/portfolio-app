import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


var   routesAppBarbkColor = new Color.fromRGBO(142, 74, 126, 1.0);
var   routesAppBarTitle   = new Text('Qual o seu destino?');
var   routesElevation     = 50.0;
const routesMapType       = MapType.normal;
var   routesMapZoom       = 15.0;
const routesMapsLatLng    = LatLng(-19.912998, -43.940933);