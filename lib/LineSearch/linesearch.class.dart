import 'package:flutter/material.dart';
import 'linesearch.variables.dart';

//LINES PAGE`S START//

AppBar linesAppBar() {
return new AppBar(
          centerTitle: linesCenterTitle,
          title: linesTitle,
          backgroundColor: linesAppBarbkColor,
           );
          }

AppBar linesChildAppBar(){
return new AppBar(
          backgroundColor: linesChildAppBarbkColor,
          primary: linesPrimary,
          title: linesTextField,
          );
        }          

Positioned linesPositioned(){
  return new Positioned(
          top:   linesPositionedTop,
          left:  linesPositionedLeft,
          right: linesPositionedRight,
          child: linesChildAppBar(),
           );
}

//LINES PAGE`S END//