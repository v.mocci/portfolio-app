import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

bool linesCenterTitle         = false;
var  linesTitle               = new Text('Linhas e paradas');
var  linesAppBarbkColor       = Color.fromRGBO(142, 74, 126, 1.0);
var  linesChildAppBarbkColor  = Color.fromRGBO(255, 255, 255, 1.0);
bool linesPrimary   = false;
var  linesTextField = TextField(
          decoration: InputDecoration(
          hintText: "Pesquisar linhas",
          border: InputBorder.none,
          hintStyle: TextStyle(color: Colors.grey)),
            );
var linesPositionedTop    = 20.0;
var linesPositionedLeft   = 20.0;
var linesPositionedRight  = 20.0;
