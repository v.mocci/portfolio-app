import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

var  rechargeAppBarbkColor      = new Color.fromRGBO(142, 74, 126, 1.0);
bool rechargeCenterTitle        = false;
var  rechargeTitle              = new Text('Seus Cartões BHBus');
var  rechargeIconbkColor        = new Color.fromRGBO(255, 255, 255, 1.0);
var  rechargeListViewEdgeInsets = new  EdgeInsets.all(20);
//card container modal variable's start//
var rechargeContainerHeight                  = 150.0;
var rechargeContainerBorderWidth             = 10.0;
var rechargeContainerImage                   = new AssetImage('assets/images/img02.jpg');
var rechargeContainerBorderColor             = new Color.fromRGBO(255, 255, 255, 5);
var rechargeContainerBorderRadiusTopLeft     = new Radius.circular(20.0);
var rechargeContainerBorderRadiusTopRight    = new Radius.circular(20.0);
var rechargeContainerBorderRadiusBottomLeft  = new Radius.circular(20.0);
var rechargeContainerBorderRadiusBottomRight = new Radius.circular(20.0);
var rechargeContainerCardText                = new Center(child: Text('Cartão A',style: TextStyle(color: const Color.fromRGBO(255, 255, 255, 1)),));
//card container modal variable's end//