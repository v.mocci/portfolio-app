import 'package:flutter/material.dart';
import '../bhbusrecharge.class.dart';

class RechargePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: rechargeAppBar(),
      body: rechargeListView(),
    );
  } 
}
