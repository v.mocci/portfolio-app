import 'package:flutter/material.dart';
import 'bhbusrecharge.variables.dart';

//RECHARGE PAGE`S START//

AppBar rechargeAppBar(){
return new AppBar(
        centerTitle:      rechargeCenterTitle,
        title:            rechargeTitle,
        backgroundColor:  rechargeAppBarbkColor,
        actions: <Widget>[
        rechargeIconButtonAppBar(),
        ],
      );
}

IconButton rechargeIconButtonAppBar(){
  return new IconButton(
  onPressed: () {},
  icon: Icon(
  Icons.add,
  color: rechargeIconbkColor,
    ), 
  );
}

ListView rechargeListView(){
return new ListView(
  padding: rechargeListViewEdgeInsets,
  children: <Widget>[
       rechargeCardContainer(),
       rechargeCardContainer(),
       rechargeCardContainer(),   
       rechargeSubTextContainer(),   
          
        ],
      
);
}

Container rechargeCardContainer(){
return new Container(
            height: rechargeContainerHeight,
            decoration: new BoxDecoration(
            border: new Border.all(
            width: rechargeContainerBorderWidth,
            color:rechargeContainerBorderColor),
            borderRadius: new BorderRadius.only(
            topLeft:     rechargeContainerBorderRadiusTopLeft,
            topRight:    rechargeContainerBorderRadiusTopRight,
            bottomLeft:  rechargeContainerBorderRadiusBottomLeft,
            bottomRight: rechargeContainerBorderRadiusBottomRight),
            image: DecorationImage(
            image: rechargeContainerImage,
            fit: BoxFit.fill,
                )),
            child: rechargeContainerCardText,
          );
}

Container rechargeSubTextContainer(){
  return new Container(
            child: const Center(child: Text('Toque em um cartão'),),);
}

//RECHARGE PAGE`S END//