# Aplicação em Flutter Language

Aplicativo para portfólio pessoal em Flutter.

## Getting Started - instalação e dependências necessárias

Este projeto atualmente utiliza as seguintes tecnologias:

- Flutter (Download: https://flutter.dev/docs/get-started/install);

Documentação oficial do Flutter para mais dúvidas:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab);
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook);
- [online documentation](https://flutter.dev/docs);

## Descrição sobre a arquitetura Front End

A aplicação teve sua estrutura front end desenvolvida com base em arquitetura limpa hexagonal. A comunicação acontece da seguinte forma:

- A estrutura separa, por pasta, os principais menus/funcionalidades;
- Dentro de cada pasta principal temos a seguinte divisão: uma pasta para as telas e dois arquivos de estilização, sendo o .class.dart e .variables.dart;
- O arquivo Class trata do esqueleto dos componentes exibidos na tela;
- O arquivo Variables trata da estilização dos campos e formatação dos dados;
- O arquivos comunicam-se entre si dentro da pasta do menu, tendo interação fora disso apenas com o main.dart que exibe as telas;
